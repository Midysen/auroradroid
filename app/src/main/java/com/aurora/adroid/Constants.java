/*
 * Aurora Droid
 * Copyright (C) 2019, Rahul Kumar Patel <whyorean@gmail.com>
 *
 * Aurora Droid is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Aurora Droid is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Aurora Droid.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.adroid;

public class Constants {
    public static final String SHARED_PREFERENCES_KEY = "com.aurora.store";
    public static final String TAG = "A-Droid";
    public static final String REPO_BASE = "https://mirror.cyberbits.eu/fdroid/repo/";
    public static final String IMG_URL_PREFIX = "/icons-640/";
    public static final String SIGNED_FILE_NAME = "index-v1.jar";
    public static final String DATA_FILE_NAME = "index-v1.json";
    public static final String REPO_AVAILABLE = "REPO_AVAILABLE";
    public static final String DATABASE_AVAILABLE = "DATABASE_AVAILABLE";
    public static final String DATABASE_LATEST = "DATABASE_LATEST";
    public static final String APP_SHARE_URL = "https://f-droid.org/app/";
    public static final String FILES = "Files";
    public static final String JAR = ".jar";
    public static final String JSON = ".json";

    public static final String PREFERENCE_BLACKLIST_APPS_LIST = "PREFERENCE_BLACKLIST_APPS_LIST";
    public static final String PREFERENCE_FAVOURITE_LIST = "PREFERENCE_FAVOURITE_LIST";
    public static final String PREFERENCE_INCLUDE_SYSTEM = "PREFERENCE_INCLUDE_SYSTEM";
    public static final String PREFERENCE_INSTALLATION_AUTO = "PREFERENCE_INSTALLATION_AUTO";
    public static final String PREFERENCE_INSTALLATION_TYPE = "PREFERENCE_INSTALLATION_TYPE";
    public static final String PREFERENCE_INSTALLATION_METHOD = "PREFERENCE_INSTALLATION_METHOD";
    public static final String PREFERENCE_INSTALLATION_DELETE = "PREFERENCE_INSTALLATION_DELETE";
    public static final String PREFERENCE_NOTIFICATION_TOGGLE = "PREFERENCE_NOTIFICATION_TOGGLE";
    public static final String PREFERENCE_NOTIFICATION_PRIORITY = "PREFERENCE_NOTIFICATION_PRIORITY";
    public static final String PREFERENCE_NOTIFICATION_PROVIDER = "PREFERENCE_NOTIFICATION_PROVIDER";
    public static final String PREFERENCE_UPDATES_INTERVAL = "PREFERENCE_UPDATES_INTERVAL";
    public static final String PREFERENCE_UI_THEME = "PREFERENCE_UI_THEME";
    public static final String PREFERENCE_UI_TRANSPARENT = "PREFERENCE_UI_TRANSPARENT";
    public static final String PREFERENCE_DOWNLOAD_DIRECTORY = "PREFERENCE_DOWNLOAD_DIRECTORY";
    public static final String PREFERENCE_DOWNLOAD_WIFI = "PREFERENCE_DOWNLOAD_WIFI";
    public static final String PREFERENCE_DOWNLOAD_ACTIVE = "PREFERENCE_DOWNLOAD_ACTIVE";
    public static final String PREFERENCE_DOWNLOAD_DEBUG = "PREFERENCE_DOWNLOAD_DEBUG";
    public static final String PREFERENCE_DOWNLOAD_STRATEGY = "PREFERENCE_DOWNLOAD_STRATEGY";
    public static final String PREFERENCE_DOWNLOAD_DELTAS = "PREFERENCE_DOWNLOAD_DELTAS";
    public static final String PREFERENCE_ENABLE_PROXY = "PREFERENCE_ENABLE_PROXY";
    public static final String PREFERENCE_PROXY_HOST = "PREFERENCE_PROXY_HOST";
    public static final String PREFERENCE_PROXY_PORT = "PREFERENCE_PROXY_PORT";
    public static final String PREFERENCE_PROXY_TYPE = "PREFERENCE_PROXY_TYPE";
}
