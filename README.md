<img src="https://i.imgur.com/kSApIjL.png" height="256" alt="Aurora Logo"><br/><img src="https://www.gnu.org/graphics/gplv3-88x31.png" alt="GPL v3 Logo">

# Aurora Droid
Aurora Droid aka A-Droid is a F-Droid client for android, based on latest MD guidelines, it aims to provide an elegant design with great UX

# Screenshots

<img src="https://gitlab.com/AuroraOSS/AuroraDroid/raw/master/fastlane/metadata/android/en-US/phoneScreenshots/ss001.png" height="400"><img src="https://gitlab.com/AuroraOSS/AuroraDroid/raw/master/fastlane/metadata/android/en-US/phoneScreenshots/ss002.png" height="400">
<img src="https://gitlab.com/AuroraOSS/AuroraDroid/raw/master/fastlane/metadata/android/en-US/phoneScreenshots/ss003.png" height="400"><img src="https://gitlab.com/AuroraOSS/AuroraDroid/raw/master/fastlane/metadata/android/en-US/phoneScreenshots/ss004.png" height="400">
<img src="https://gitlab.com/AuroraOSS/AuroraDroid/raw/master/fastlane/metadata/android/en-US/phoneScreenshots/ss005.png" height="400"><img src="https://gitlab.com/AuroraOSS/AuroraDroid/raw/master/fastlane/metadata/android/en-US/phoneScreenshots/ss006.png" height="400">

# Features

* Free/Libre software
  -- Has GPLv3 licence

* Beautiful design
  -- Built upon latest Material Design guidelines

* Better repo management
  -- You can select your choice of repositories from a built in list & easy add new repositories by scanning QR code

`Aurora Droid is still in development! Bugs are to be expected! Any bug reports are appreciated`

# Links
* AndroidFileHost - [Downloads](https://androidfilehost.com/?w=files&flid=294487)
* XDA Forum - [Thread](https://forum.xda-developers.com/android/apps-games/app-aurora-droid-fdroid-client-t3932663)
* XDA Labs - [Link](https://labs.xda-developers.com/store/app/com.aurora.adroid)
* Support Group - [Telegram](https://t.me/AuroraDroid)

# Aurora Droid uses the following Open Source libraries:

* [RX-Java](https://github.com/ReactiveX/RxJava)
* [ButterKnife](https://github.com/JakeWharton/butterknife)
* [OkHttp3](https://square.github.io/okhttp/)
* [Glide](https://github.com/bumptech/glide)
* [Fetch2](https://github.com/tonyofrancis/Fetch)

